import colors from 'vuetify/es5/util/colors'

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      { src: '/js/fb-sdk.js' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    // './plugins/vue-google-oauth2'
    // './plugins/vue-facebook'
    // './plugins/social.js'
    { src: '~plugins/social.js', ssr: true }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
// Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    // Multi language config
    ['nuxt-i18n', {
      locales: [
        {
          name: 'Chinese',
          code: 'cn',
          file: 'cn-CN.js'
        },
        {
          name: 'English',
          code: 'en',
          file: 'en-US.js'
        },
      ],
      langDir: 'lang/',
      lazy : true,
      defaultLocale: 'en',
    }]
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    proxy: true,
    prefix: '/api/',
    credentials: true
    // See https://github.com/nuxt-community/axios-module#options
  },
  proxy:{
    '/api/':{
      target:"http://127.0.0.1:8000/api/",
      pathRewrite: {
        '^/api': ''
      }
    }
  },
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    // transpile: ['vue-google-oauth2'],
    // transpile: ['vue-facebook'],
    // transpile: ['social'],
    extend (config, ctx) {
    }
  },

  //facebook

}
