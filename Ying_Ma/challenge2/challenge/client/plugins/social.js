import Vue from 'vue'

import "font-awesome/css/font-awesome.css"
import SocialSharing from 'vue-social-sharing'

Vue.use(SocialSharing)
