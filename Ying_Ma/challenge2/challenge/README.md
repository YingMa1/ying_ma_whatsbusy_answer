# install dependencies
$ npm install

# launch Django server (Suppose you have Python3 and pip)
* $ pip install pipenv
* $ pipenv shell
* $ pipenv install django django-rest-framework django-cors-headers django-phone-field
* $ cd server
* $ python manage.py runserver
*  http://127.0.0.1:8000/admin  
* superuser: maying password:maying123456

# client with hot reload at localhost:3000
* $ cd client
* $ npm run dev
* http://localhost:3000/

# generate static project
* $ npm run generate
