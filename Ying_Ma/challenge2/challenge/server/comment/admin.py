from django.contrib import admin

# Register your models here.
from .models import Comment  # add this
# Register your models here.
class CommentAdmin(admin.ModelAdmin):
    list_display = ['name', 'text', 'created_time', 'post']
    fields = ['name', 'text']

admin.site.register(Comment,CommentAdmin)