from blog.models import Post
from django.contrib import messages
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from .forms import CommentForm
from django.forms.utils import ErrorDict
from rest_framework.utils import json
@csrf_exempt
def comments(request, slug):
    post = get_object_or_404(Post, slug=slug)
    if request.method == 'POST':
        form = CommentForm(request.POST or None)
        response = {}
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.save()
            #messages.add_message(request, messages.SUCCESS, 'comment successfully added', extra_tags='success')
            response = {"SUCCESS" : "Successfully upload comment"}
            return JsonResponse(response)
        #messages.add_message(request, messages.ERROR, 'comment failed', extra_tags='danger')
        response = {"ERROR" : "failed to upload comments", "Slug" : slug}
        ErrorDict=form.errors
        Error_Str=json.dumps(ErrorDict)
        Error_Dict=json.loads(Error_Str)
        return JsonResponse(response)

    if request.method == 'GET':
        comment_list = post.comment_set.all().order_by('-created_time').values()
        return JsonResponse(list(comment_list), safe=False)