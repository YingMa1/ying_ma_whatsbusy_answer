from .forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic
# sign up
from rest_framework import permissions
from rest_framework.generics import CreateAPIView
from django.contrib.auth import get_user_model # If used custom user model
from rest_framework_simplejwt.views import TokenObtainPairView
from .serializers import UserSerializer
from api.CustomJWTSerializer import CustomJWTSerializer
from django.views.decorators.csrf import csrf_exempt
#google sign in
from rest_framework.views import APIView

# from forms import MyRegistrationForm
# from django.contrib.formtools.wizard.views import SessionWizardView
# from django.core.mail import send_mail

# from celery.result import AsyncResult
# from celery_test.tasks import do_something_long
# from django.core.urlresolvers import reverse
# from django.utils import simplejson as json
# new sign u

from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.hashers import make_password
from rest_framework.utils import json
from rest_framework.response import Response
import requests
from rest_framework_simplejwt.tokens import RefreshToken
#from django.contrib.auth.models import User
from .models import NewUser as User
from django.shortcuts import render
# new sign up
class CreateUserView(CreateAPIView):

    model = get_user_model()
    permission_classes = [
        permissions.AllowAny # Or anon users can't register
    ]
    serializer_class = UserSerializer
    def post(self, request):
        data = request.data
        if 'error' in data:
            content = {'message': 'wrong happen in sign up.'}
            return Response(content)
        try:
            user = User.objects.get(email=data['email'])
        except User.DoesNotExist:
            user = User()
            user.username = data['username']
            user.email = data['email']
            # provider random default password
            user.set_password(data['password'])


            user.save()
            token = RefreshToken.for_user(user)  # generate token without username & password
            response = {}
            response['username'] = user.username
            response['access_token'] = str(token.access_token)
            response['refresh_token'] = str(token)
            return Response(response)


        response = {}
        response['error'] = "Email has been used!"
        return Response(response)


#
# def register_and_subscribe(request):
#     if request.method == "POST":
#         form = CustomUserForm(request.POST)
#         if form.is_valid():
#             try:
#                 user = form.save()
#                 customer = strip.Charge.create(
#                 email = user.email
#                 #need register plan on the stripe
#                 plan = 'HHPL01'
#                 card = user.strip_id
#                 )
#                 user.strip_id = customer.id
#                 user.plan = 'HHPL01'
#                 user.save()
#                 return redirect('/user/login')
#              except strip.CardError:
#                 form.add_my_error("The card has been declined")
#         else:
#             form = CustomUserForm()
#         args['form'] = form
#         args['publishable'] = setting.STRIPE_PUBLISHABLE
#         args['months'] = range(1,12)
#         args['years'] = range(2020,2030)
#         args['soon'] = datetime.date.today() + datetime.timedelta(days=30)
#
#         return render_to_response('register_and_subscribe.html',args)

class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = CustomJWTSerializer

#google login
class GoogleView(APIView):
    def post(self, request):
        data = request.data
        if 'error' in data:
            content = {'message': 'wrong google token / this google token is already expired.'}
            return Response(content)

        # create user if not exist
        try:
            user = User.objects.get(email=data['email'])
        except User.DoesNotExist:
            user = User()
            user.username = data['username']
            # provider random default password
            user.password = make_password(BaseUserManager().make_random_password())
            user.email = data['email']
            user.save()

        token = RefreshToken.for_user(user)  # generate token without username & password
        response = {}
        response['username'] = user.username
        response['access_token'] = str(token.access_token)
        response['refresh_token'] = str(token)
        return Response(response)