from django.contrib import admin
from .models import NewUser
# Register your models here.
from django.contrib.auth.models import PermissionsMixin

class NewUserAdmin(admin.ModelAdmin):
    list_display = ['username','email','is_active','is_superuser','date_joined','password']
   # fields = ['username','email', 'gender', 'birthday','phone','school','major','educationlevel','password']

admin.site.register(NewUser,NewUserAdmin)



# def save_model(self, request, obj, form, change):
#      obj.username = request.user
#      super().save_model(request, obj, form, change)