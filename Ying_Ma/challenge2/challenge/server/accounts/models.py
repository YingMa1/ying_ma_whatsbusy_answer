from django.db import models

# Create your models here.
from django.contrib.auth.models import AbstractUser, AbstractBaseUser, BaseUserManager
from django.db import models
from django.contrib.auth.models import PermissionsMixin

class NewUser(AbstractUser):
#     GENDER_CHOICES = (
#                     ('male','男'),
#                     ('female','女'),
#                 )
#    gender = models.CharField(max_length=6,choices=GENDER_CHOICES,default='男')
    username = models.CharField(max_length=60,unique = True)
    email = models.EmailField(max_length=60)

    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    #password = models.CharField(max_length=60)

    # USERNAME_FIELD = 'username'
    # REQUIRED_FIELDS = ['email']
    def __str__(self):
        return self.email



    def has_module_perms(self,app_label):
        return True

    #password = models.CharField(max_length=100)

#     def save_model(self, request, obj, form, change):
#             if request.user.groups.filter(name='Encoder').exists():
#                 pass
#             else:
#                 return super(NewUser, self).save_model(request, obj, form, change)


