from django import forms
from django.contrib.auth.forms import UserCreationForm
#from django.contrib.auth.models import User
from .models import NewUser
from django.db import models

class UserCreationForm(UserCreationForm):
    email = forms.EmailField(required=True, label='Email')

#     gender = models.CharField(max_length=20)
#     birthday = models.CharField(max_length=100)
#     phone = models.CharField(max_length=100,default='00000000')

    class Meta:
       # model = User
        fields = ("username", "email", "password", "password2")

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]

        if commit:
            user.save()
        return user