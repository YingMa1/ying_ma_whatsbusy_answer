from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.models import User
from django.db.models import Q
from .models import NewUser

User = get_user_model()
#user =  NewUser()

class AuthBackend(ModelBackend):
    supports_object_permissions = True
    supports_anonymous_user = False
    supports_inactive_user = False

    def get_user(self, user_id):
        try:
            return NewUser.objects.get(pk=user_id)
        except NewUser.DoesNotExist:
            return None

    def authenticate(self, request, username=None, password=None):
        print('inside custom auth')
        try:
            user = NewUser.objects.get(
                Q(username=username) | Q(email=username) )
            print(user)
        except NewUser.DoesNotExist:
            return None
        print(user)
        if NewUser.check_password(password):
             return user
        else:
            return None