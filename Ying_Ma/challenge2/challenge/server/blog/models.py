from django.db import models
from django.template.defaultfilters import slugify

from django.contrib.auth.models import User
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class Post(models.Model):
    avatar = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    subtitle = models.CharField(max_length=100)
    slug = models.SlugField(unique=True) # use this field to uniquely identify a blog
    content = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    author = models.CharField(max_length=100)
    #hit_count = models.DecimalField(max_digits=19, decimal_places=10)
    hit_count = models.PositiveIntegerField(default=0)
    #统计阅读数量
    def increase_views(self):
            self.hit_count += 1
            self.save(update_fields=['hit_count'])

    def __str__(self):
            return self.title

        # 自定义 get_absolute_url 方法
        # 记得从 django.urls 中导入 reverse 函数
    def get_absolute_url(self):
        return reverse('blog:detail', kwargs={'slug': self.slug})




    def __unicode__(self):
        return self.content

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super(Post, self).save(*args, **kwargs)