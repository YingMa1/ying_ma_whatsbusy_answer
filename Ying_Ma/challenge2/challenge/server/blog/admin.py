from django.contrib import admin

# Register your models here.
from .models import Post

#class PostAdmin(admin.ModelAdmin):
#    list_display = ['title', 'created_time', 'modified_time', 'category', 'author']

# 把新增的 PostAdmin 也注册进来
#admin.site.register(Post, PostAdmin)
class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'created_on', 'author', 'slug']
    fields = ['avatar','title','subtitle','slug', 'content', 'author','hit_count']

admin.site.register(Post,PostAdmin)



def save_model(self, request, obj, form, change):
    obj.author = request.user
    super().save_model(request, obj, form, change)