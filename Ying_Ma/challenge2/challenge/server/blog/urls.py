from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views

#urlpatterns = [
 #   path(r'^$', views.index, name='index'),
  #  #新增绑定不同文章的id Blog/1/、 Blog/255/ 等都是符合规则的
   # path(r'^Blog/(?P<pk>[0-9]+)/$', views.detail, name='detail'),

#]
app_name = 'blog'
urlpatterns = [
    path('', views.index, name='index'),
    path('<slug:slug>', views.detail, name='detail'),
]