from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render
from rest_framework import permissions
from rest_framework import viewsets
from rest_framework.generics import CreateAPIView
from rest_framework.utils import json
from rest_framework.response import Response
from .models import Post
from .forms import BlogForm
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect

from django.template.defaultfilters import slugify
from django.forms.utils import ErrorDict

'''def index(request):
    res = {
                "url": "/blog/media/图片名字.jpg"   # 路径
            }
    return render(request, 'blog/index.html', context={
                          'title': '我的博客首页',
                          'welcome': '欢迎访问我的博客首页'
                      })
def detail(request, pk):
    post = get_object_or_404(Blog, pk=pk)
    post.increase_views()
    return render(request, 'blog/detail.html', context={'post': post})
'''


@csrf_exempt
def index(request):

    if request.method == 'POST':
        form = BlogForm(request.POST)

        slug = request.POST.get('slug')
        form.fields['slug'].choices = [(slug, slug)]

        if form.is_valid():

            post = form.save()
            post.save()
            response = {'success': 'successfully saved'}
            return JsonResponse(response)


        ErrorDict=form.errors
        Error_Str=json.dumps(ErrorDict)
        Error_Dict=json.loads(Error_Str)
        return HttpResponse(Error_Dict.values())


    if request.method == 'GET':
        blog_list = Post.objects.all().order_by('-created_on').values()  # reverse order using '-'
        return JsonResponse(list(blog_list), safe=False)

def detail(request, slug):
    post = Post.objects.get(slug=slug)
   # post = get_object_or_404(Blog, slug=slug)
    post.increase_views()
    # print(post.pk, post.title, post.slug)
    #response = {'pk': post.pk, 'title': post.title, 'slug': post.slug, 'content': post.content}
    response = {'title': post.title, 'slug': post.slug, 'content': post.content,'hit_count':post.hit_count,'created_on':post.created_on}
    return JsonResponse(response)