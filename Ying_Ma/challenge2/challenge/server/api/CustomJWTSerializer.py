from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework import serializers
from django.contrib.auth.models import User
from accounts.models import NewUser

class CustomJWTSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(NewUser)
        # Add custom claims
        token['username'] = user.username

        #token['gender'] = user.gender
        #token['birthday'] = user.birthday
        #token['phone'] = user.birthday
        token['password'] = user.password

        return token
    def validate(self, attrs):
        credentials = {
            'username': '',
            #'gender':'',
            #'birthday':'',
            #'phone':'',
            'password': attrs.get("password")
        }


        # This is answering the original question, but do whatever you need here.
        # For example in my case I had to check a different model that stores more user info
        # But in the end, you should obtain the username to continue.
        user_obj = NewUser.objects.filter(email=attrs.get("username")).first() or NewUser.objects.filter(username=attrs.get("username")).first()
        if user_obj:
            credentials['username'] = user_obj.username
        data = super().validate(credentials)
        data['username'] = self.user.username
        return data
