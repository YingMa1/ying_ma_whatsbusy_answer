from django.db import models
from django.core.validators import RegexValidator
from phone_field import PhoneField
# Create your models here.
class Consult(models.Model):
    name = models.CharField(max_length=120)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone = models.CharField(validators=[phone_regex], max_length=17, blank=True) # validators should be a list
    email = models.EmailField()
    question = models.TextField()

