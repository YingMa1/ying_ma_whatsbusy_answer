# core/urls.py
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import ConsultViewSet

router = DefaultRouter()
router.register(r'data', ConsultViewSet)

urlpatterns = [
    path("", include(router.urls))
]