from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from .serializers import ConsultSerializer
from .models import Consult

class ConsultViewSet(viewsets.ModelViewSet):
    serializer_class = ConsultSerializer
    queryset = Consult.objects.all()